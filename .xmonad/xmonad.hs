import XMonad hiding ((|||))
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks (manageDocks, avoidStruts, docksEventHook)
import XMonad.Util.Run (spawnPipe)
import XMonad.Util.EZConfig (additionalKeys,additionalKeysP,removeKeys)
import System.IO
import System.Exit
import XMonad.Actions.CycleWS (nextWS, prevWS, moveTo, shiftToNext, shiftToPrev, Direction1D (Next, Prev), WSType (WSIs))
import XMonad.Actions.Navigation2D (windowGo, windowSwap, switchLayer, withNavigation2DConfig, Direction2D ( D,U,L,R ))
import XMonad.Layout.LayoutCombinators ((|||))
import XMonad.Actions.CycleSelectedLayouts 
import XMonad.StackSet (tag, focusDown, RationalRect (..))
import XMonad.Layout.ResizableTile (MirrorResize (MirrorShrink, MirrorExpand), ResizableTall (..)) 
import XMonad.ManageHook
import XMonad.Hooks.EwmhDesktops
import XMonad.Util.NamedScratchpad (namedScratchpadManageHook, customFloating,
                                    namedScratchpadAction, NamedScratchpad ( NS ))
import Graphics.X11.ExtraTypes.XF86               


myModMask = mod4Mask      -- Rebind Mod to the Windows key
altMask = mod1Mask
myTerminal = "urxvt"
myNormalBorderColor = "#FF8A00"
myFocusedBorderColor = "#0011FF"
myBGcolor = "#2f343f"

-- Colours
fg        = "#ebdbb2"
bg        = "#282828"
gray      = "#a89984"
bg1       = "#3c3836"
bg2       = "#505050"
bg3       = "#665c54"
bg4       = "#7c6f64"

green     = "#b8bb26"
darkgreen = "#98971a"
red       = "#fb4934"
darkred   = "#cc241d"
yellow    = "#fabd2f"
blue      = "#83a598"
purple    = "#d3869b"
aqua      = "#8ec07c"
white     = "#eeeeee"

pur2      = "#5b51c9"
blue2     = "#2266d0"


main = do
    xmproc <- spawnPipe "/home/tgaref/.local/bin/xmobar /home/tgaref/.xmobarrc"


    xmonad $ ewmh $
      withNavigation2DConfig def $
      def
        { manageHook = myManageHook
        , layoutHook = avoidStruts $ myLayout
        , handleEventHook = mconcat
                  [ docksEventHook                 
                  , handleEventHook def
                  , fullscreenEventHook]
        , startupHook = myStartupHook
        , logHook = dynamicLogWithPP xmobarPP
                        { ppOutput = hPutStrLn xmproc
                        , ppTitle = xmobarColor "green" "" . shorten 50
                        }
        , modMask = myModMask
        , terminal = myTerminal
        , normalBorderColor  = myNormalBorderColor
        , focusedBorderColor = myFocusedBorderColor

        }
        `removeKeys`
        [ (myModMask             , xK_space)
        , (myModMask .|. shiftMask, xK_q)
        ]          
        `additionalKeys`
        [ ((controlMask           , xK_Print), spawn "sleep 0.2; scrot -s")
        , ((0                     , xK_Print), spawn "scrot")
        , ((0                     , xF86XK_AudioLowerVolume), spawn "amixer set Master 5%-")
        , ((0                     , xF86XK_AudioRaiseVolume), spawn "amixer set Master 5%+")
        , ((0                     , xF86XK_AudioMute), spawn "amixer set Master toggle")
        , ((0                     , xK_F12), spawn "amixer set Master toggle")
        , ((0                     , xK_F3), namedScratchpadAction scratchpads "ranger")
        , ((0                     , xK_F2), namedScratchpadAction scratchpads "tmux")
        , ((0                     , xK_F1), spawn "rofi -show run -lines 5 -theme Paper -font 'Noto Sans Mono 18'")
        , ((altMask               , xK_Tab), spawn "rofi -show window -theme Paper -font 'Noto Sans Mono 18'")
        , ((altMask .|. controlMask  , xK_Left  ), moveTo Prev (WSIs notSP))
        , ((altMask .|. controlMask  , xK_Right ), moveTo Next (WSIs notSP))
        , ((controlMask .|. shiftMask, xK_Right),  shiftToNext)
        , ((controlMask .|. shiftMask, xK_Left),   shiftToPrev)
        , ((myModMask             , xK_l ),    cycleThroughLayouts ["Full", "ResizableTall"])
   -- Switch between layers
        , ((myModMask,                 xK_n), windows focusDown)
   -- Directional navigation of windows
        , ((myModMask,                 xK_Right), windowGo R False)
        , ((myModMask,                 xK_Left ), windowGo L False)
        , ((myModMask,                 xK_Up   ), windowGo U False)
        , ((myModMask,                 xK_Down ), windowGo D False)
   -- Swap adjacent windows
        , ((myModMask .|. shiftMask, xK_Right), windowSwap R False)
        , ((myModMask .|. shiftMask, xK_Left ), windowSwap L False)
        , ((myModMask .|. shiftMask, xK_Up   ), windowSwap U False)
        , ((myModMask .|. shiftMask, xK_Down ), windowSwap D False)

        ] `additionalKeysP`
        [ ("M-S-k", sendMessage MirrorShrink)
        , ("M-S-j", sendMessage MirrorExpand)
        , ("M-S-h", sendMessage Shrink)
        , ("M-S-l", sendMessage Expand)
        , ("M-r e", spawn "emacsclient -c")
        , ("M-r b", spawn "firefox")
        , ("M-r u", spawn "urxvtc")
        , ("M-r t", spawn "alacritty --config-file /home/tgaref/.config/alacritty/alacritty.yml")
        , ("M-S-q", spawn "/home/tgaref/local/bin/rofi-system.fish")
        ] where notSP = (return $ ("NSP" /=) . tag) :: X (WindowSpace -> Bool)


--------------------------------------------------------------------------------
-- LAYOUT                                                                   
--------------------------------------------------------------------------------


myLayout = avoidStruts $ Full ||| ResizableTall nmaster delta ratio []
  where
    nmaster = 1      -- The default number of windows in the master pane
    ratio   = 1/2    -- Default proportion of screen occupied by master pane
    delta   = 3/100  -- Percent of screen to increment by when resizing panes

--------------------------------------------------------------------------------
-- MANAGEHOOK                                                                   
--------------------------------------------------------------------------------

myManageHook = composeAll $
  [ manageDocks
  , manageNamedScratchPad
  , manageApps
  , manageHook def
  ]

manageApps = composeAll
    [ className =? "firefox"         --> doShift "2"
    , className =? "Google-chrome"   --> doShift "2"
    , className =? "Emacs"           --> doShift "3"
    , className=? "Gnome-calculator" --> doFloat
    , className=? "window1"          --> doFloat
    , className=? "skype"            --> doFloat
    ]

manageNamedScratchPad :: ManageHook
manageNamedScratchPad = namedScratchpadManageHook scratchpads

scratchpads = [ NS "ranger" "urxvtc -e ranger" (title =? "ranger")
                 (customFloating $ RationalRect (1/10) (1/40) (4/5) (3/4))
              , NS "tmux" "urxvtc -e tmux" (title =? "tmux")
                 (customFloating $ RationalRect (1/10) (1/40) (4/5) (4/5))
              ]

--------------------------------------------------------------------------------
-- STARTUPHOOK                                                                   
--------------------------------------------------------------------------------

myStartupHook :: X ()
myStartupHook = do
  spawn "picom"
  spawn "trayer --edge top --align right --SetDockType true --SetPartialStrut true --expand true --width 6 --transparent true --alpha 0 --tint 0x2f343f  --height 22"
  spawn "nm-applet"
  spawn "megasync"
  spawn "dropbox start"
  spawn "xfce4-power-manager"
  spawn "udiskie"
  spawn "setxkbmap -layout \"us,gr\" -option grp:win_space_toggle -option grp_led:scroll :2"
  spawn "xset r rate 200 30"
  spawn "xsetroot -cursor_name left_ptr"
  spawn "hsetroot -fill /usr/share/backgrounds/gnome/Stripes.jpg"
--  spawn "hsetroot -fill /home/tgaref/Pictures/arch-logo.jpg"
  spawn "urxvtd"
  spawn "emacs --daemon"
 
